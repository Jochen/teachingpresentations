
<!-- .slide: data-background="./static/images/fiber_titlebg.png" -->

# Pedagogical Project: Numerical exercises for MCC046
## Jochen Schröder

---

# Aims

Redesign numerical exercises in MCC046 Photonics and Lasers from step-by-step 
instructions to more explorative exercises and accompanying web-applications.


## Desired outcome

Better physical intuitions of the students about fundamental phenomena in 
optics/photonics, such as free-space propagation and diffraction, 
optical resonantors and devices.

---

# Background

----

## MCC046

- Compulsory course in the Masters in Wireless Photonics and Space Engineering 
- First optics course in the program
- 7.5 credits
- 14 Lectures, 4 Numerical excercises + 4 home assignments, 2 Lab exercises
- Considered to have high workload, but generally liked

----

### Previous exercises

- previous excercises were created > 4 years ago
- the creator has left Chalmers
- Matlab based
- very descriptive instructions 
- focus on implementing the formula
- little "sanity" testing
- little information on "pitfalls" of the simulations

----

# Explorative learning with applications

- Interactive engagement can significantly enhance student understanding compared
to traditional lecture approaches [1]
- Online tools are an effective and scalable way to extend instructor capabilities [2]

<div class="font-serif static text-xs bottom-0 left-0 text-left leading-tight">

<p style="margin-top:20px;margin-bottom:5px !important">
[1]R. R. Hake, American Journal of Physics, vol. 66, no. 1, pp. 64–74, Jan. 1998, doi: 10.1119/1.18809.
</p>

<p style="margin: 0px;!important">
[2]C. Wieman, Change: The Magazine of Higher Learning, vol. 39, no. 5, pp. 9–15, Jan. 2007, doi: 10.3200/CHNG.39.5.9-15.
</p>

</div>


---

# Methods

- Convert the traditional step-by-step instructions to more general questions
where students have to find their own solutions
- Convert exercises to Python using Jupyter Notebooks (interactive programming environment in the browser)
- Design and build web-applications that allow easy manipulation and exploration
of effects through simulated propagation

----

## Example

### First HA
#### Previous version
- simulation of free-space propagation via diffraction 
- provide "legacy" method
- present formula for "new, better" method
- provide some examples to simulate

----

##### Problems

- "New" method is not "faultless"
- implementing the formula numerically is easy
- choosing the correct simulation parameters (e.g. discretisation) is difficult
- no insight into how simulations can fail


----

##### Changes 

- focus on learing about failure modes
- learn to detect failure modes
- reduction of simulations to implement

##### Desired Outcomes

- understanding of pitfalls of simulation
- ability to  spot issues ("what is realistic")

---

# Evaluation

Evaluating the effect/success of the changes seems to be the most challenging

#### Current plan

- add specific questions to student survey
- perform survey/quiz at end of every exercise

---

# Current status

- Focus on "technical" work
- Rewritten 1 of 4 home assignments
- implemented one web-application 

----

<iframe class="w-full object-scale-down" data-src="https://mybinder.org/v2/gl/Jochen%2Fphotonicslasers/HEAD?urlpath=%2Fproxy%2F5006%2Ffourier_filtering" height="700" />

---

# Questions

- are there better ways to evaluate?
- should I count access to web-apps?

---

# Thank you for your feedback



